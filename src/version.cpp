/*
 * version.cpp:
 * version: pre-compile versioning tool
 * Copyright (c) 2018-2019 Justin Whitmore
 ***********************************************************************
 * This file is part of version:
 *
 * AutoPutty is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AutoPutty is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
*/
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <fstream>
#include <time.h>

#ifdef _WIN32
#define popen _popen
#define pclose _pclose
#endif

std::string PROGRAM_NAME = "";

std::string getStdoutFromCommand(std::string cmd)
{
  std::string data;
  FILE * stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);
  }
  if (data == "") { data = "-1"; }
  return data;
}

std::string getBuildNumber()
{
  time_t rawtime;
  struct tm timeinfo;
  char buffer[80];

  time(&rawtime);
  localtime_s(&timeinfo, &rawtime);

  strftime(buffer, 80, "%y%j", &timeinfo);
  std::ostringstream os;
  os << buffer;
  return os.str();
}

std::string getSubversionRevision()
{
  std::string revision;
  revision = getStdoutFromCommand("svnversion . --no-newline");
  std::size_t found = revision.find(":");
  if (found != std::string::npos) {
    revision = revision.substr((++found), std::string::npos);
  }
  if (revision == "") { revision = "-1"; }
  std::cout << "revision: " << revision << std::endl;
  return revision;
}

std::string getGitRevision()
{
  std::string revision;
  revision = getStdoutFromCommand("git rev-parse --short HEAD");
  if (revision == "") { revision = "-1"; }
  revision.erase(std::remove(revision.begin(), revision.end(), '\n'), revision.end());
  revision.erase(std::remove(revision.begin(), revision.end(), '\r'), revision.end());
  std::cout << "revision: " << revision << std::endl;
  return revision;
}

bool replace(std::string& str, const std::string& from, const std::string& to) 
{
  std::size_t start_pos = str.find(from);
  if (start_pos == std::string::npos) {
    return false;
  }
  str.replace(start_pos, from.length(), to);
  return true;
}
 
int writeFile(const std::string& infile, const std::string& outfile, const std::string revision, const std::string build, const std::string major, const std::string minor, const std::string patch)
{
  std::ifstream in;
  std::ofstream out;
  in.open(infile.c_str(), std::ifstream::in);
  out.open(outfile.c_str(), std::ofstream::out);

  if (!in.is_open()) {
    std::cout << "Error opening file " << infile << std::endl;
    return 0;
  }
  if (!out.is_open()) {
    std::cout << "Error opening file " << outfile << std::endl;
    return 0;
  }

  if (in.is_open()) {
    std::string line;
    while (getline(in, line))
    {
      replace(line, "$HEADER1$", "This is an auto-generated file");
      replace(line, "$HEADER2$", "This file should NOT be added to a repository");
      replace(line, "$REV$", revision);
      replace(line, "$BUILD$", build);
      replace(line, "$MAJOR$", major);
      replace(line, "$MINOR$", minor);
      replace(line, "$PATCH$", patch);
      if (out.is_open()) {
        out << line << std::endl;
      }
    }
  }
  in.close();
  out.close();
  return 0;
}

int main(int argc, char* argv[])
{
  PROGRAM_NAME = argv[0];
  std::string infile;
  std::string outfile;
  std::string vcs;
  std::string major;
  std::string minor;
  std::string patch;

  if (argc < 4)
  {
    std::cout << "Error not enough parameters" << std::endl;
    std::cout << "Usage: " << argv[0] << " -i <infile> -o <outfile> [-vcs <svn/git>] [-major <#>] [-minor <#>] [-patch <#>]" << std::endl;
    return 1;
  }

  for (int i = 1; i < argc; i++) {
    if (std::string(argv[i]) == "-i") {
      if (i+1 < argc) {
        infile = argv[++i];
        continue;
      } else {
        std::cerr << argv[i-1] << " option requires argument." << std::endl;
        return 1;
      }
    }
    if (std::string(argv[i]) == "-o") {
      if (i + 1 < argc) {
        outfile = argv[++i];
        continue;
      }
      else {
        std::cerr << argv[i - 1] << " option requires argument." << std::endl;
        return 1;
      }
    }
    if (std::string(argv[i]) == "-vcs") {
      if (i + 1 < argc) {
        vcs = argv[++i];
        continue;
      }
      else {
        std::cerr << argv[i - 1] << " option requires argument." << std::endl;
        return 1;
      }
    }
    if (std::string(argv[i]) == "-major") {
      if (i + 1 < argc) {
        major = argv[++i];
        continue;
      }
      else {
        std::cerr << argv[i - 1] << " option requires argument." << std::endl;
        return 1;
      }
    }
    if (std::string(argv[i]) == "-minor") {
      if (i + 1 < argc) {
        minor = argv[++i];
        continue;
      }
      else {
        std::cerr << argv[i - 1] << " option requires argument." << std::endl;
        return 1;
      }
    }
    if (std::string(argv[i]) == "-patch") {
      if (i + 1 < argc) {
        patch = argv[++i];
        continue;
      }
      else {
        std::cerr << argv[i - 1] << " option requires argument." << std::endl;
        return 1;
      }
    }
  }

  if (infile == "" || outfile == "") {
    std::cout << "Error not enough parameters" << std::endl;
    std::cout << "Usage: " << argv[0] << " -i <infile> -o <outfile> [-vcs <svn/git>] [-major <#>] [-minor <#>] [-patch <#>]" << std::endl;
    return 1;
  }

  for (int i = 1; i < argc; i++) {
    std::cout << "ARG " << i << " [" << argv[i] << "]/[" << argc << "]" << std::endl;
  }
  std::string revision = "-1";
  if (vcs == "git") {
    revision = getGitRevision();
  } else if (vcs == "svn") {
    revision = getSubversionRevision();
  } else if (vcs == "") {
    revision = "";
  }
  writeFile(infile, outfile, revision, getBuildNumber(), major, minor, patch);
  return 0;
}

